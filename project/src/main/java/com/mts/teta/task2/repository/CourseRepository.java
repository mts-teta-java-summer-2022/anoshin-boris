package com.mts.teta.task2.repository;

import com.mts.teta.task2.errorhandler.NoCourseFoundException;

import java.util.List;
import java.util.Optional;

public interface CourseRepository {
    List<Course> findAll();

    List<Course> findByTitleWithPrefix(String prefix);

    Optional<Course> findById(Long id);

    void save(Course course);

    void deleteById(Long id) throws NoCourseFoundException;
}