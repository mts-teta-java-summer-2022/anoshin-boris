package com.mts.teta.task2.service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EnTitleValidator implements TitleValidator{
    private final TitleValidator origin;

    @Override
    public boolean validate(String title) {
        if (origin.validate(title)) {
            String[] strings = title.split("\\s");
            String[] stringsUpperCase = title.toUpperCase().split("\\s");

            if (title.matches(".*[А-Яа-я].*")) {
                return false;
            }
            if (strings[0].charAt(0) != stringsUpperCase[0].charAt(0)) {
                return false;
            }
            if (strings[strings.length-1].charAt(0) != stringsUpperCase[stringsUpperCase.length-1].charAt(0)) {
                return false;
            }
            for (int i = 1; i < strings.length-1; i++) {
                if (strings[i].charAt(0) != stringsUpperCase[i].charAt(0)) {
                    if (!strings[i].matches("a|but|for|or|not|the|an")) {
                        return false;
                    }
                }
                else {
                    if (strings[i].toLowerCase().matches("a|but|for|or|not|the|an")) {
                        return false;
                    }
                }
            }
            return true;
        }
        else {
            return false;
        }
    }
}
