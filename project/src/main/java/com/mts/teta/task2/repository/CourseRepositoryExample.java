package com.mts.teta.task2.repository;

import com.mts.teta.task2.errorhandler.NoCourseFoundException;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
public class CourseRepositoryExample implements CourseRepository {
    private final Map<Long, Course> courses = new ConcurrentHashMap<>();

    @PostConstruct
    public void Init() {
        save(new Course("The gunslinger", "Steven King"));
        save(new Course("Hobbit OR there and back again.", "J.R.R. Tolkien"));
    }

    @Override
    public List<Course> findAll() {
        return courses.values()
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Course> findById(Long id) {
        return courses.containsKey(id) ? Optional.ofNullable(courses.get(id)) : Optional.empty();
    }

    @Override
    public void save(Course course) {
        if (courses.containsKey(course.getId())){
            courses.get(course.getId()).setAuthor(course.getAuthor());
            courses.get(course.getId()).setTitle(course.getTitle());
        }
        else {
            courses.put(course.getId(), course);
        }
    }

    @Override
    public void deleteById(Long id) throws NoCourseFoundException {
        if (courses.containsKey(id)){
            courses.remove(id);
        }
        else {
            throw new NoCourseFoundException("No course found");
        }
    }

    @Override
    public List<Course> findByTitleWithPrefix(String prefix) {
        return courses.values()
                .stream()
                .filter(course -> course.getTitle().startsWith(prefix))
                .collect(Collectors.toList());
    }
}