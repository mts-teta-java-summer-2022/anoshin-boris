package com.mts.teta.task2.validate;

import com.mts.teta.task2.service.AnyTitleValidator;
import com.mts.teta.task2.service.EnTitleValidator;
import com.mts.teta.task2.service.RuTitleValidator;
import com.mts.teta.task2.service.TitleValidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TitleCaseValidator implements ConstraintValidator<TitleCase, String> {
    private TitleType type;

    @Override
    public void initialize(TitleCase constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
        this.type = constraintAnnotation.type();
    }

    @Override
    public boolean isValid(String title, ConstraintValidatorContext cxt) {
        return validate(title, type);
    }

    private boolean validate(String title, TitleType type) {
        TitleValidator titleValidator;

        if (type.equals(TitleType.EN)) {
            titleValidator = new EnTitleValidator(new AnyTitleValidator());
        }
        else if (type.equals(TitleType.RU)) {
            titleValidator = new RuTitleValidator(new AnyTitleValidator());
        }
        else {
            titleValidator = new AnyTitleValidator();
        }

        return titleValidator.validate(title);
    }
}
