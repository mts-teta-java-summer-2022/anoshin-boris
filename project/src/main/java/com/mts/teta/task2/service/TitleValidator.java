package com.mts.teta.task2.service;

public interface TitleValidator {
    boolean validate (String title);
}