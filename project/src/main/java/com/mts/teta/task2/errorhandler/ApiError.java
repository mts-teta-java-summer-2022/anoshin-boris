package com.mts.teta.task2.errorhandler;

import lombok.Data;
import lombok.NonNull;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class ApiError {
    @NonNull private String message;
    @NonNull private String dateOccurred;

    public ApiError(String message) {
        this.message = message;
        this.dateOccurred = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }
}