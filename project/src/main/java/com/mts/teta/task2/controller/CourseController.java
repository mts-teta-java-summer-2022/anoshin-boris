package com.mts.teta.task2.controller;

import com.mts.teta.task2.errorhandler.NoCourseFoundException;
import com.mts.teta.task2.repository.Course;
import com.mts.teta.task2.request.CourseRequestToCreate;
import com.mts.teta.task2.request.CourseRequestToUpdate;
import com.mts.teta.task2.service.CourseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/course")
@RequiredArgsConstructor
public class CourseController {
    private final CourseService service;

    @GetMapping
    public List<Course> courseTable() {
        return service.courseTable();
    }

    @GetMapping("/{id}")
    public Course getCourse(@PathVariable("id") Long id) {
        return service.getCourse(id);
    }

    @GetMapping("/filter")
    public List<Course> getCoursesByTitlePrefix(@RequestParam(name = "titlePrefix", required = false) String titlePrefix) {
        return service.getCoursesByTitlePrefix(titlePrefix);
    }

    @PutMapping("/{id}")
    public void updateCourse(@PathVariable Long id,
                             @Valid @RequestBody CourseRequestToUpdate request) {
        service.updateCourse(id, request);
    }

    @PostMapping
    public Course createCourse(@Valid @RequestBody CourseRequestToCreate request) {
        return service.createCourse(request);
    }

    @DeleteMapping("/{id}")
    public void deleteCourse(@PathVariable Long id) throws NoCourseFoundException {
        service.deleteCourse(id);
    }
}