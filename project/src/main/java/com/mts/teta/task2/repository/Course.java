package com.mts.teta.task2.repository;

import lombok.Data;
import lombok.NonNull;

import java.util.concurrent.atomic.AtomicLong;

@Data
public class Course {
    @NonNull private String author;
    @NonNull private Long id;
    @NonNull private String title;
    private static AtomicLong nextId = new AtomicLong();

    public Course(@NonNull String title, @NonNull String author) {
        this.author = author;
        this.title = title;
        this.id = nextId.incrementAndGet();
    }
}