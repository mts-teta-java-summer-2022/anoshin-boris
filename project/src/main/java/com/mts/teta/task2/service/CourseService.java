package com.mts.teta.task2.service;

import com.mts.teta.task2.errorhandler.NoCourseFoundException;
import com.mts.teta.task2.repository.Course;
import com.mts.teta.task2.repository.CourseRepository;
import com.mts.teta.task2.request.CourseRequestToCreate;
import com.mts.teta.task2.request.CourseRequestToUpdate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static java.util.Objects.requireNonNullElse;

@Service
@RequiredArgsConstructor
public class CourseService {
    private final CourseRepository courseRepository;

    public List<Course> courseTable() {
        return courseRepository.findAll();
    }

    public Course getCourse(Long id) {
        return courseRepository.findById(id).orElseThrow();
    }

    public List<Course> getCoursesByTitlePrefix(String titlePrefix) {
        return courseRepository.findByTitleWithPrefix(requireNonNullElse(titlePrefix, ""));
    }

    public void updateCourse(Long id, CourseRequestToUpdate request) {
        Course course = courseRepository.findById(id).orElseThrow(() -> new NoCourseFoundException("No course found"));
        course.setTitle(request.getTitle());
        course.setAuthor(request.getAuthor());
        courseRepository.save(course);
    }

    public Course createCourse(CourseRequestToCreate request) {
        Course course = new Course(request.getTitle(), request.getAuthor());
        courseRepository.save(course);
        return course;
    }

    public void deleteCourse(Long id) throws NoCourseFoundException {
        courseRepository.deleteById(id);
    }
}
