package com.mts.teta.task2.request;

import com.mts.teta.task2.validate.TitleCase;
import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotBlank;
import static com.mts.teta.task2.validate.TitleType.EN;

@Getter
@Setter
public class CourseRequestToCreate extends Request {
    @NotBlank(message = "Course author has to be filled")
    private String author;

    @TitleCase(type = EN)
    @NotBlank(message = "Course title has to be filled")
    private String title;
}