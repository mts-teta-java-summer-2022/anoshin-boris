package com.mts.teta.task2.request;

import com.mts.teta.task2.validate.TitleCase;
import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotBlank;
import static com.mts.teta.task2.validate.TitleType.RU;

@Getter
@Setter
public class CourseRequestToUpdate extends Request {
    @NotBlank(message = "Course author has to be filled")
    private String author;

    @NotBlank(message = "Course title has to be filled")
    @TitleCase(type = RU, message = "Title has to be kinda russian")
    private String title;
}