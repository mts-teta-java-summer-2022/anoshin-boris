package com.mts.teta.task2.service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RuTitleValidator implements TitleValidator{
    private final TitleValidator origin;

    @Override
    public boolean validate(String title) {
        if (origin.validate(title)) {
            String[] strings = title.split("\\s");
            String[] stringsUpperCase = title.toUpperCase().split("\\s");

            if (title.matches(".*[A-Za-z].*")) {
                return false;
            }
            if (strings[0].charAt(0) != stringsUpperCase[0].charAt(0)) {
                return false;
            }
            for (int i = 1; i < strings.length; i++) {
                if (strings[i].charAt(0) == stringsUpperCase[i].charAt(0)) {
                    return false;
                }
            }
            return true;
        }
        else {
            return false;
        }
    }
}
