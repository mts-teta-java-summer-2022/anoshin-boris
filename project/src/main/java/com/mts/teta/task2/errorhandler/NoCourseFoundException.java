package com.mts.teta.task2.errorhandler;

public class NoCourseFoundException extends RuntimeException {
    public NoCourseFoundException(String errorMessage) {
        super(errorMessage);
    }
}