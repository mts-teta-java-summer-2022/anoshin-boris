package com.mts.teta.task2.service;

public class AnyTitleValidator implements TitleValidator {
    @Override
    public boolean validate(String title) {

        if (title.matches("(?s).*[\n\r\t].*")) {
            return false;
        }

        if (title.matches("(?s).*[\\s]{2}.*")) {
            return false;
        }

        if (title.matches("^\\s.*|.*\\s$")) {
            return false;
        }

       if (title.matches(".*[a-zA-Z].*") && title.matches(".*[а-яА-Я].*")) {
            return false;
        }

       if (title.matches(".*[^a-zA-Zа-яА-я\"\'\\,\\:\\s].*")) {
            return false;
        }

        return true;
    }
}