package com.mts.teta.task2.validate;

public enum TitleType {
    RU,
    EN,
    ANY
}