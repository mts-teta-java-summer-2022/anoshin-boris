package com.mts.teta.task1;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class EnrichmentService {
    private final MessageValidator validator;
    private final ConcurrentHashMap<String, User> msisdnUserHashMap;
    public static final List<Message> invalidResponseList = Collections.synchronizedList(new ArrayList<>());
    public static final List<Message> validResponseList = Collections.synchronizedList(new ArrayList<>());

    public EnrichmentService() {
        this.validator = new MessageValidator();
        this.msisdnUserHashMap = new ConcurrentHashMap<>();

        for (int i = 0; i < 10; i++) {
            msisdnUserHashMap.put(String.valueOf(i), new User("Name"+i, "SecondName"+i));
        }
    }

    public synchronized Message enrich(Message message) {
        if (message.getEnrichmentType() == Message.EnrichmentType.MSISDN) {
            if(!validator.isJSONValid(message)) {
                invalidResponseList.add(message);
                return message;
            }
            try {
                message.setEnrichment(msisdnUserHashMap.get(message.getMSISDN()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            validResponseList.add(message);
            return message;
        }
        invalidResponseList.add(message);
        return message;
    }
}