package com.mts.teta.task1;

import org.json.JSONException;
import org.json.JSONObject;

public class MessageValidator {
    public boolean isJSONValid(Message message)  {
        try {
            new JSONObject(message.getContent());
        } catch (JSONException ex) {
            return false;
        }
        //В JSON должно быть поле msisdn со строковым значением. Остальные поля произвольны.
        try {
            JSONObject object = new JSONObject(message.getContent());
            String msisdn = (String) object.get("msisdn");
            //поле msisdn отсутствует или информация не найдена
            if (msisdn.equals("")) return false;
        }
        catch (Exception e) {
            return false;
        }
        return true;
    }
}
