package com.mts.teta.task1;

import lombok.Data;
import lombok.NonNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

@Data
public class Message {
    @NonNull private String content;
    @NonNull private EnrichmentType enrichmentType;
    final Random random = new Random();

    public enum EnrichmentType {
        MSISDN
    }

    public Message(String msisdn, int i) {
        this.content = MessageContent.createContent(msisdn, i);
        this.enrichmentType = EnrichmentType.MSISDN;
    }

    public void setEnrichment(User user) throws JSONException {
        JSONObject object = new JSONObject(this.getContent());

        try {
            JSONObject enrichment = object.getJSONObject("enrichment");
            enrichment.put("firstName", user.getFirstName());
            enrichment.put("lastName", user.getLastName());
        } catch (Exception e) {
            JSONObject enrichment = new JSONObject();
            enrichment.put("lastName", user.getLastName());
            enrichment.put("firstName", user.getFirstName());
            object.accumulate("enrichment", enrichment);
        }

        this.content = object.toString();
    }

    public String getMSISDN () {
        JSONObject object;
        String msisdn = "";
        try {
            object = new JSONObject(this.content);
            msisdn = String.valueOf(object.get("msisdn"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msisdn;
    }
}
