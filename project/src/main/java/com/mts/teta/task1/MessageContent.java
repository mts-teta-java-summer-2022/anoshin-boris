package com.mts.teta.task1;

public class MessageContent {
    public static String createContent(String msisdn, int i) {
        String content;
        switch(i) {
            case 0:
                content = "{\n" +
                        "    \"action\": \""+i+"\",\n" +
                        "    \"page\": \"book_card\",\n" +
                        "    \"msisdn\": \"" + msisdn + "\"\n" +
                        "}";
                break;
            case 1:
                content = "{\n" +
                        "    \"action\": \""+i+"\",\n" +
                        "    \"page\": \"book_card\",\n" +
                        "    \"id\": \"" + msisdn + "\"\n" +
                        "}";
                break;
            default:
                content = "{\n" +
                        "    \"action\": \""+i+"\",\n" +
                        "    \"page\": \"book_card\",\n" +
                        "    \"msisdn\": \""+ msisdn +"\",\n" +
                        "    \"enrichment\": {\n" +
                        "        \"firstName\": \"Petya\",\n" +
                        "        \"lastName\": \"Petrov\"\n" +
                        "    }\n" +
                        "}";
                break;
        }
        return content;
    }
}
