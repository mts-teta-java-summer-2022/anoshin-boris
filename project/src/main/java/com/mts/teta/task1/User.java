package com.mts.teta.task1;

import lombok.Data;
import lombok.NonNull;

@Data
public class User {
    @NonNull private String firstName;
    @NonNull private String lastName;
}
