select count(*) from profile t1
where not exists (select 1 from post t2 where t2.profile_id = t1.profile_id);

select post_id from post t1
where (select count(1) from comment t2 where t2.post_id = t1.post_id)=2
	and substr(t1.title,1,1) in ('0','1','2','3','4','5','6','7','8','9')
	and LENGTH(t1.content)>20
order by 1 asc;

select post_id from post t1
where (select count(1) from comment t2 where t2.post_id = t1.post_id)<2
order by 1 asc
LIMIT 3;