package com.mts.teta.task2;

import com.mts.teta.task1.Message;
import com.mts.teta.task1.MessageValidator;
import com.mts.teta.task2.service.AnyTitleValidator;
import com.mts.teta.task2.service.EnTitleValidator;
import com.mts.teta.task2.service.RuTitleValidator;
import com.mts.teta.task2.service.TitleValidator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Task2Test {
    //ANY
    @Test
    public void validateAnyTitle_1() {
        AnyTitleValidator validator = new AnyTitleValidator();
        assertTrue(validator.validate("'War and Peace: stressed or desserts'"));
    }
    @Test
    public void validateAnyTitle_2() {
        AnyTitleValidator validator = new AnyTitleValidator();
        assertTrue(validator.validate("\"Война, мир\""));
    }
    @Test
    public void validateAnyTitleRule1_1() {
        AnyTitleValidator validator = new AnyTitleValidator();
        assertFalse(validator.validate("War\rand\rPeace"));
    }
    @Test
    public void validateAnyTitleRule1_2() {
        AnyTitleValidator validator = new AnyTitleValidator();
        assertFalse(validator.validate("War\nand\nPeace"));
    }
    @Test
    public void validateAnyTitleRule1_3() {
        AnyTitleValidator validator = new AnyTitleValidator();
        assertFalse(validator.validate("War\tand\tPeace"));
    }
    @Test
    public void validateAnyTitleRule2_1() {
        AnyTitleValidator validator = new AnyTitleValidator();
        assertFalse(validator.validate("War  and Peace"));
    }
    @Test
    public void validateAnyTitleRule3_1() {
        AnyTitleValidator validator = new AnyTitleValidator();
        assertFalse(validator.validate(" War and Peace"));
    }
    @Test
    public void validateAnyTitleRule3_2() {
        AnyTitleValidator validator = new AnyTitleValidator();
        assertFalse(validator.validate("War and Peace "));
    }
    @Test
    public void validateAnyTitleRule4_1() {
        AnyTitleValidator validator = new AnyTitleValidator();
        assertFalse(validator.validate("This is a wonderful день"));
    }
    @Test
    public void validateAnyTitleRule5_1() {
        AnyTitleValidator validator = new AnyTitleValidator();
        assertFalse(validator.validate("This is a wonderful day!"));
    }
    //EN
    @Test
    public void validateEnTitle_1() {
        TitleValidator validator = new EnTitleValidator(new AnyTitleValidator());
        assertTrue(validator.validate("This Is a Wonderful Day"));
    }
    @Test
    public void validateEnTitle_2() {
        TitleValidator validator = new EnTitleValidator(new AnyTitleValidator());
        assertFalse(validator.validate("Чудо день"));
    }
    @Test
    public void validateEnTitleRule1_1() {
        TitleValidator validator = new EnTitleValidator(new AnyTitleValidator());
        assertFalse(validator.validate("This Is a Wonderful day"));
    }
    @Test
    public void validateEnTitleRule1_2() {
        TitleValidator validator = new EnTitleValidator(new AnyTitleValidator());
        assertFalse(validator.validate("this Is a Day"));
    }
    @Test
    public void validateEnTitleRule2_1() {
        TitleValidator validator = new EnTitleValidator(new AnyTitleValidator());
        assertFalse(validator.validate("This Is A Wonderful Day"));
    }
    @Test
    public void validateEnTitleRule2_2() {
        TitleValidator validator = new EnTitleValidator(new AnyTitleValidator());
        assertFalse(validator.validate("This is a Wonderful Day"));
    }
    //RU
    @Test
    public void validateRuTitle_1() {
        TitleValidator validator = new RuTitleValidator(new AnyTitleValidator());
        assertTrue(validator.validate("Чудо день"));
    }
    @Test
    public void validateRuTitle_2() {
        TitleValidator validator = new RuTitleValidator(new AnyTitleValidator());
        assertFalse(validator.validate("This Is a Wonderful Day"));
    }
    @Test
    public void validateRuTitleRule1_1() {
        TitleValidator validator = new RuTitleValidator(new AnyTitleValidator());
        assertFalse(validator.validate("чудо день"));
    }
    @Test
    public void validateRuTitleRule1_2() {
        TitleValidator validator = new RuTitleValidator(new AnyTitleValidator());
        assertFalse(validator.validate("Чудо День"));
    }
}
