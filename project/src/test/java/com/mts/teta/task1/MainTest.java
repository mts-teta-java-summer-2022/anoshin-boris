package com.mts.teta.task1;

import com.mts.teta.task1.EnrichmentService;
import com.mts.teta.task1.Message;
import com.mts.teta.task1.MessageValidator;
import org.json.JSONException;
import org.junit.jupiter.api.Test;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

//  Testing Non-Concurrent Parts
  @Test
  public void testValidMessage() {
    MessageValidator messageValidator = new MessageValidator();
    Message message = new Message(String.valueOf(1), 0);
    assertTrue(messageValidator.isJSONValid(message));
  }

  @Test
  public void testValidMessage2() {
    MessageValidator messageValidator = new MessageValidator();
    Message message = new Message(String.valueOf(1), 2);
    assertTrue(messageValidator.isJSONValid(message));
  }

  @Test
  public void testInvalidMessage() {
    MessageValidator messageValidator = new MessageValidator();
    Message message = new Message(String.valueOf(1), 1);
    assertFalse(messageValidator.isJSONValid(message));
  }

  @Test
  public void testGetMSISDN() {
    Random random = new Random();
    String msisdn = String.valueOf(random.nextInt(100));
    Message message = new Message(msisdn, 0);
    assertEquals(msisdn, message.getMSISDN());
  }

  @Test
  public void testEnrich() throws JSONException {
    EnrichmentService enrichmentService = new EnrichmentService();
    Message message = new Message(String.valueOf(1), 1);
    org.skyscreamer.jsonassert.JSONAssert.assertEquals(message.getContent(), enrichmentService.enrich(message).getContent(),true);
  }

//  Testing Concurrent Parts (End-to-End тест)

  @Test
  public void testRandomMessageSet() throws InterruptedException {
    int numberOfThreads = 10;
    Random random = new Random();
    ExecutorService service = Executors.newFixedThreadPool(numberOfThreads);
    CountDownLatch latch = new CountDownLatch(numberOfThreads);
    EnrichmentService enrichmentService = new EnrichmentService();
    for (int i = 0; i < numberOfThreads; i++) {
      service.execute(() -> {
        Message response = enrichmentService.enrich(new Message(String.valueOf(random.nextInt(10)), random.nextInt(3)));
        latch.countDown();
      });
    }
    latch.await();
    assertEquals(numberOfThreads, EnrichmentService.invalidResponseList.size()+EnrichmentService.validResponseList.size());
  }
}